# Text Frequency Analysis

Text Frequency Analyzer. 
## requirements

- pymorphy2

- pandas

### USE 

    python3 analize.py <input_text_file_name> <output_text_file_name>

# conc_files.py

Concatenates files to one file.

## USE
    python3 conc_files.py <file names list>

#!/usr/bin/env python3
# coding: utf-8
#import modules
import sys
import pymorphy2
import pandas as pd

#Checking CLI syntax
if(len(sys.argv) != 3):
	print(
		"""\033[31mERROR!: bad syntax\033[0m 
TRY: python3 analize.py <input_file_path> <output_file_path>"""
		)
	exit(1)

input_file_name = sys.argv[1]
output_file_name = sys.argv[2]

#create MorphAnalyzer
morph = pymorphy2.MorphAnalyzer()

#Dict for words count
f_dic = {}

#open file for read only. File path from CLI arguments
f = open(input_file_name, 'r')

#only filtred words
words = []

#read lines from file
for line in f :
	#skip empty lines
	if(len(line) < 2) :
		continue
	#convert to lower case
	clean = line.lower()
	clean = clean.strip()
	#remove punctuation symbols
	clean = "".join(c for c in clean if c not in ('!','?','.','…',',',':',';','-', '\'', '\"', '«', '»', '—'))
	#remove other trash simbols
	clean = "".join(c for c in clean if c not in ('[', ']', '(', ')', '{', '}'))
	clean = clean.replace("  ", " ") 
	clean = clean.replace("  ", " ")
	clean = clean.replace("\xa0", " ")

	#spliting line to words
	temp = clean.split(" ");

	#filtering and normalize words
	for word in temp :
		#skip short words before normalization
		if(len(word) < 3) :
			continue
		normal = morph.parse(word)[0].normal_form
		#skip short words after normalization
		if(len(normal) < 3) :
			continue
		words.append(normal)

f.close()
#total words count
print(len(words))

#counts words frequency
for word in words :
	if(word in f_dic) :
		f_dic[word][0] += 1
	else :
		f_dic[word] = [1, len(word)]


## make DataFrame
arr = []
for word in f_dic :
	tup = (word, f_dic[word][0])
	arr.append(tup)

df = pd.DataFrame(arr)
df.columns = ["word", "freq"]

#other way
#df = pd.DataFrame.from_dict(f_dic, orient="index")
#df.columns = ["freq", "len"]


#sort words by frequency
df = df.sort_values(by=['freq', 'word'], ascending=False)

#open file for write results
f = open(output_file_name, 'w')

#write results to file
for index, row in df.iterrows() :
	s = "" + row["word"] + " - " + str(row["freq"])
	f.write(s + "\n");


print(df.head(20))
print("TOTAL WORDS:", df.shape[0])
